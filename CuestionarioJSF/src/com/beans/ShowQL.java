package com.beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.modelo.JdbcConexion;
import com.objetos.Cuestionario;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;



public class ShowQL {
		
	private String user = "0"; // nombre delusuario
	
	private List<Cuestionario> lista = new ArrayList<Cuestionario>(); //definimos la lista de cuestionarios
	
	public void setUser(String iuser){

		this.user=iuser;
	}
	
	public String getUser(){
		
		return user;
	}
	
	public List<Cuestionario> getLista(){
		return lista;
	}
	
	public void setLista(List<Cuestionario> lista) {
		this.lista = lista;
	}
	
	//recuperar los datos de la consulta
	public void Listar(){
		user = "list";
		Connection myconexion = new JdbcConexion().init();
		
		if (myconexion != null) {
			try {
				String sql = "Select * From questionnaire where id>0 order by id asc";
				
				Statement st = myconexion.createStatement();
				ResultSet rs = st.executeQuery(sql);
				Integer i=0;
				
				lista.clear();	
				while (rs.next()) {
					i = i + 1;
					Cuestionario Qt = new Cuestionario();
					Qt.setNro(rs.getString("id"));
					Qt.setTitulo(rs.getString("title"));
					lista.add(Qt);
				} 
				user = i.toString();
				//return lista;
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} else {
			//return error;
		}
		//return error;
	}
	
	public String redirec(){
		return "recargar";
	}
	
}
