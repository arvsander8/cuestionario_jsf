package com.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.*;

import com.funciones.Funciones;

@ManagedBean
public class Autocompletar {

	private String label;
	private String value;
	private String tabla;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getTabla() {
		return tabla;
	}
	public void setTabla(String tabla) {
		this.tabla = tabla;
	}
	
	public List<String> complete(String query) {
		List<String> results = new ArrayList<String>();
		results = new Funciones().getQuestionnaireList(query);
		return results;
	}
	
	
}
