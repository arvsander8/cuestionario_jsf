package com.beans;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.faces.bean.ManagedBean;

import com.modelo.JdbcConexion;
@ManagedBean
public class RegistrarBean {

	private String nombre;
	private String apellido1;
	private String apellido2;
	private String fecha;
	private String email;
	private String nick;
	private String pass1;
	private String pass2;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getPass1() {
		return pass1;
	}
	public void setPass1(String pass1) {
		this.pass1 = pass1;
	}
	public String getPass2() {
		return pass2;
	}
	public void setPass2(String pass2) {
		this.pass2 = pass2;
	}
	
	
	public String setRegistro(){
		Connection myconexion = new JdbcConexion().init();

		if (myconexion != null) {

			String seq = "select max(id) as id from \"user\"";

			try {
				Statement st = myconexion.createStatement();
				ResultSet rs = st.executeQuery(seq);
				if (rs.next()) {
					String id = rs.getString("id");
					PreparedStatement pst = myconexion.prepareStatement("INSERT into \"user\"  VALUES (?,?,?,?,?,?,?,?,?)");

					pst.setInt(1, Integer.parseInt(id)+1);
					pst.setInt(2, Integer.parseInt(id)+1);
					pst.setString(3, nombre);
					pst.setString(4, apellido1);
					pst.setString(5, apellido2);
					
					//Para setear las fechas
					SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
					java.util.Date  fec = formato.parse(fecha);
					Date sqlfec = new Date(fec.getTime());
					pst.setDate(6, sqlfec);
					
					pst.setString(7, pass1);
					pst.setInt(8, 1);
					pst.setString(9, nick);

					int numRowsChanged = pst.executeUpdate();
					pst.close();
					if (numRowsChanged != 0) {
						return "Se registro Correctamente";
					} else {
						 return "Fallo el Registro";
					}
					

				} else {
					return "Ocurrio un error en el servidor";
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} else {
			return "Conexion insatisfactoria a la base de datos";
		}
		return "Ocurrio algun error";

	}
	
	
}
