package com.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.*;

@ManagedBean
public class Navigator {

	private String[] results = { "uno", "dos", "tres" };
	private String texto;
	private Date date;
	
	public Date getDate(){
		return this.date;
	}
	public void setDate(Date dt){
		this.date = dt;
	}
	
	public String getTexto(){
		return this.texto;
	}
	public void setTexto(String tx){
		this.texto = tx;
	}
	
	public String escoger() {
		return results[2];

	}

	public List<String> complete(String query) {
		List<String> results = new ArrayList<String>();
		for (int i = 0; i < 10; i++)
			results.add(query + i);
		return results;
	}
}
